phpMyAdmin
==========


1. Build: `docker build --rm -t phpmyadmin .`

1. Copy `env.list.template` to `env.list` and update

1. Run:

```
docker run -t -i -p 80:80 --env-file=env.list \
-h phpmyadmin --restart="on-failure:10" \
--link beservices:beservices --name phpmyadmin phpmyadmin \
/bin/bash -c "supervisord; export > /env; bash"
```

1. Disconnect with `ctrl-p` `ctrl-q`
